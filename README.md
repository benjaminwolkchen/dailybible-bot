# dailybible-bot
This bot is set up using https://gitlab.com/yogthos/mastodon-bot.

The config.edn file in this repository is already preconfigured for this bot, all you have to do is replace the Twitter and Mastodon Tokens at the top of the file.

To get the ``access_token_key`` and ``access_token_secret`` values I used http://v21.io/iwilldancetheoauthdanceforyou/

After the bot is configured, run ``mastodon-bot /path/to/config.edn`` to check if it's working.

If everything works, you can configure to run the bot automattically (every 30 Minutes) by typing ``crontab -e``.

A text file should open. Go to the last line in the text file and add the following and adjust the path to your config.edn to run the bot automattically every 30 Minutes:

```
*/30 * * * * mastodon-bot /path/to/bot/config.edn
```

To contact me - for help, or anything really - write me a DM on my Mastodon Profile: [@hbenjamin@mstdn.social](https://mstdn.social/@hbenjamin).